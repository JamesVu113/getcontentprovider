package xyz.james.getcontentprovider;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TRUONGBEO_PROVIDER_URI = "content://com.truongbeo.provider/staff";
    private static final String TRUONGBEO_COLUMN_NAME = "name";
    private static final String TRUONGBEO_COLUMN_ID = "_id";
    private static final String TRUONGBEO_COLUMN_SCORE = "score";
    private static final String TRUONGBEO_COLUMN_RANK = "rank";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printDataFromTruongBeoProvider();
            }
        });
    }

    private void printDataFromTruongBeoProvider() {
        Uri staff = Uri.parse(TRUONGBEO_PROVIDER_URI);
        Cursor c = getContentResolver().query(staff, null, null, null, TRUONGBEO_COLUMN_NAME);

        if (c == null) return;

        StringBuilder builder = new StringBuilder();
        if (c.moveToFirst()) {
            do {
                builder.append(c.getInt(c.getColumnIndex(TRUONGBEO_COLUMN_ID)))
                        .append(" ")
                        .append(c.getString(c.getColumnIndex(TRUONGBEO_COLUMN_NAME)))
                        .append(" ")
                        .append(c.getString(c.getColumnIndex(TRUONGBEO_COLUMN_SCORE)))
                        .append(" ")
                        .append(c.getString(c.getColumnIndex(TRUONGBEO_COLUMN_RANK)))
                        .append("\n");

            } while (c.moveToNext());
        }

        Toast.makeText(getApplicationContext(), builder.toString(), Toast.LENGTH_LONG).show();
    }
}
